from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import user, userdata, user_register_request, cattle_reg_appliction_req, cattle_fa, cattle
from .serializers import userserializer, cattleserializer, loginserializer
import json
import base64

from .matching import matching_nose
from .match_cow_face import match_face


# url login
class login(APIView):
    def post(self, request):
        # print(request)
        json_data = json.load(request)
        # json_data = json.load(request)
        # print(json_data)
        uname = json_data['uname']
        password = json_data['password']

        login_database = userdata.objects.all()

        for data in login_database:
            if data.uname == uname and data.password == password:
                serializer = loginserializer(data)
                return Response({"result": "True", "data": serializer.data})

        return Response({"result": "FALSE"})


# url is name
class namelist(APIView):
    def get(self, request):
        names = user.objects.all()

        serializer = userserializer(names, many=True)
        return Response(serializer.data)

    def post(self, request):
        # name = request.data['fname']
        jso = json.load(request)
        # print(jso['encoded'])
        # for a in names:
        #   if a.fname == name:
        #      return Response(testingserializer(a).data)

        faceimage = base64.b64decode(jso['encoded'])
        muzzleimage = base64.b64decode(jso['muzzle'])
        userid = jso['uid']
        # print(userid+" UID")
        # names = user.objects.all()

        name = user.objects.get(uid=userid)
        # print(name.lname)
        cows_id = name.cattle_set.all()
        print(cows_id)
        filename = 'image.jpg'  # I assume you have a way of picking unique filenames
        with open(filename, 'wb') as f:
            f.write(faceimage)

        muzzlefilename = 'muzzle.jpg'
        f = open(muzzlefilename, "wb")
        f.write(muzzleimage)

        flag_nose = None
        print(type(cows_id))
        # flag_face = match_face(filename)
        # TODO: fix directory
        flag_face = match_face(r"C:\Users\try\Desktop\haar_cascade\1\1-1.jpg")
        print(flag_face)
        if flag_face:
            for id in cows_id:
                print(str(id.cid))
                # TODO: fix directory
                #flag_nose = matching_nose(muzzlefilename, id)
                flag_nose = matching_nose("C1002.jpg", id)
                if flag_nose:
                    serializer = cattleserializer(id)
                    return Response({"result": "True", "data": serializer.data})
                    # print(True)

            # serializer = userserializer(cows_id)


            # flag_nose = matching_nose(filename)
            # if flag_nose:

            # print (serializer.data)
            if not flag_nose:
                return Response({"result": "False"})
        else:
            return Response({"result": "False"})


def test(request):
    return HttpResponse("<h1>TESTING</h1>")


# url is register
class request_register(APIView):
    def post(self, request):
        json_data = json.load(request)
        fname = json_data["fname"]
        mname = json_data["mname"]
        lname = json_data["lname"]
        mobile = json_data["mobile"]
        address = json_data["address"]

        new_data = user_register_request.objects.all()

        new_data.create(fname=fname, mname=mname, lname=lname, mobile=mobile, address=address)
        # new_data.save()

        return Response({"result": "success"})


class register_animal(APIView):
    def post(self, request):
        json_data = json.load(request)
        uid = json_data["uid"]
        breed = json_data["breed"]
        color = json_data["color"]
        horn_size = json_data["horn"]

        new_data = cattle_reg_appliction_req.objects.all()

        new_data.create(uid=uid, breed=breed, color=color, horn_size=horn_size)

        return Response({"result": "success"})
